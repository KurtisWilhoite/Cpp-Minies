# C++-Minies
C++ Miniature projects all done in around or less than a day

These are all C++ small projects, all of which have been generally completed within the same day they've been started. They're primarily to display one way of accomplishing a small task in C++.
I'm currently still in my first year of software engineering as I'm starting these, but plan on adding to it by placing new folders of completed miniature projects here as I complete them, meaning they will get better over time, but some slack should be given as I'm still quite new to programming on the whole, being so early on in my education.
